package com.yrymash.dbvehicleinsurance.services.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;

import com.yrymash.dbvehicleinsurance.brands.Brand;
import com.yrymash.dbvehicleinsurance.drivers.*;
import com.yrymash.dbvehicleinsurance.engines.Engine;

public class DriverDAO extends BaseDAO{
	
	Connection connection = null;

	public DriverDAO(){
	}
	
	public List<Person> getDriversFromDB(){
		
		Person person = null;
		List<Person> persons = new ArrayList<Person>();
		
		try{
			connection = getConnectionToDB();
            Statement statement = connection.createStatement();
            ResultSet resultPerson = statement.executeQuery("SELECT * FROM \"Driver\" order by id_driver asc");
            while(resultPerson.next()){
            	int idPerson = resultPerson.getInt("id_driver");
   	 			String firstNamePerson = resultPerson.getString("first_name_driver");
   	 			String secondNamePerson = resultPerson.getString("second_name_driver");
   	 			String birthdayOwner = resultPerson.getString("birthday_driver");
   	 			String[] arrayOfbirthdayPerson = birthdayOwner.split("-");
   	 			int yearBirthdayPerson = Integer.parseInt(arrayOfbirthdayPerson[0]);
   	 			int monthBirthdayPerson = Integer.parseInt(arrayOfbirthdayPerson[1]);
   	 			int dayBirthdayPerson = Integer.parseInt(arrayOfbirthdayPerson[2]);
   	 			LocalDate birthdayPerson = LocalDate.of(yearBirthdayPerson,monthBirthdayPerson, dayBirthdayPerson);
   	 			String dateOfOwnerLicense = resultPerson.getString("date_of_drivers_license");
   	 			String[] arrayOfDateOfOwnerLicense = dateOfOwnerLicense.split("-");
   	 			int yearDateOfOwnerLicense = Integer.parseInt(arrayOfDateOfOwnerLicense[0]);
   	 			int monthDateOfOwnerLicense = Integer.parseInt(arrayOfDateOfOwnerLicense[1]);
   	 			int dayDateOfOwnerLicense = Integer.parseInt(arrayOfDateOfOwnerLicense[2]);
   	 			LocalDate dateOfPersonLicense = LocalDate.of(yearDateOfOwnerLicense, monthDateOfOwnerLicense, dayDateOfOwnerLicense);
   	 			LocalDate today = LocalDate.now();

   	 			if(today.getYear() - dateOfPersonLicense.getYear() > 10){
   	 				person = new ProffesionalDriver(
   	 						birthdayPerson,
   	 						dateOfPersonLicense,
   	 						firstNamePerson,
   	 						secondNamePerson,
   	 						idPerson);
   	 			}else{
   	 				person = new RegularDriver(
   	 						birthdayPerson,
   	 						dateOfPersonLicense,
   	 						firstNamePerson,
   	 						secondNamePerson,
   	 						idPerson);
   	 				};
   	 			persons.add(person);
				}
            
			}catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
	        return persons;
		}
	
	public Person getOwnerById(Integer id){
		 
		 Person currentOwner = null;
		 
		 try{
			 connection = getConnectionToDB();
			 PreparedStatement preStatement = connection.prepareStatement("SELECT * FROM \"Driver\" where id_driver = ?"); 
			 preStatement.setInt(1, id);
			 ResultSet resultPerson = preStatement.executeQuery(); 
			 while(resultPerson.next()){
				 	int idPerson = resultPerson.getInt("id_driver");
				 	String firstNamePerson = resultPerson.getString("first_name_driver");
				 	String secondNamePerson = resultPerson.getString("second_name_driver");
				 	String birthdayOwner = resultPerson.getString("birthday_driver");
				 	String[] arrayOfbirthdayPerson = birthdayOwner.split("-");
				 	int yearBirthdayPerson = Integer.parseInt(arrayOfbirthdayPerson[0]);
				 	int monthBirthdayPerson = Integer.parseInt(arrayOfbirthdayPerson[1]);
				 	int dayBirthdayPerson = Integer.parseInt(arrayOfbirthdayPerson[2]);
				 	LocalDate birthdayPerson = LocalDate.of(yearBirthdayPerson,monthBirthdayPerson, dayBirthdayPerson);
				 	String dateOfOwnerLicense = resultPerson.getString("date_of_drivers_license");
				 	String[] arrayOfDateOfOwnerLicense = dateOfOwnerLicense.split("-");
				 	int yearDateOfOwnerLicense = Integer.parseInt(arrayOfDateOfOwnerLicense[0]);
				 	int monthDateOfOwnerLicense = Integer.parseInt(arrayOfDateOfOwnerLicense[1]);
				 	int dayDateOfOwnerLicense = Integer.parseInt(arrayOfDateOfOwnerLicense[2]);
				 	LocalDate dateOfPersonLicense = LocalDate.of(yearDateOfOwnerLicense, monthDateOfOwnerLicense, dayDateOfOwnerLicense);
				 	LocalDate today = LocalDate.now();

				 	if(today.getYear() - dateOfPersonLicense.getYear() > 10){
				 		currentOwner = new ProffesionalDriver(
				 				birthdayPerson,
				 				dateOfPersonLicense,
				 				firstNamePerson,
				 				secondNamePerson,
				 				idPerson);
				 	}else{
				 		currentOwner = new RegularDriver(
				 				birthdayPerson,
				 				dateOfPersonLicense,
				 				firstNamePerson,
				 				secondNamePerson,
				 				idPerson);
				 		};
		 			}
		 }catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
		 return currentOwner;
	}
	
public void insertDriverIntoDB(Person person){
		
		try{
			connection = getConnectionToDB();
			PreparedStatement preStatement = connection.prepareStatement("INSERT INTO \"Driver\""
                + "(ID_DRIVER, FIRST_NAME_DRIVER, SECOND_NAME_DRIVER, BIRTHDAY_DRIVER, DATE_OF_DRIVERS_LICENSE)" + "VALUES"
                + "(?, ?, ?, ?, ?)");
			preStatement.setInt(1, person.getId());
			preStatement.setString(2, person.getFirstName());
			preStatement.setString(3, person.getLastName());
			Integer yearOfBirthdayOwner = person.getBirthday().getYear();
			Integer monthOfBirthdayOwner = person.getBirthday().getMonthValue();
			Integer dayOfBirthdayOwner = person.getBirthday().getDayOfMonth();
			Date birthdayOwner = new Date(yearOfBirthdayOwner -1900, monthOfBirthdayOwner - 1, dayOfBirthdayOwner);
			preStatement.setDate(4, birthdayOwner);
			Integer yearOfDateOfDriverLicense = person.getDateOfDriverLicense().getYear();
			Integer monthOfDateOfDriverLicense = person.getDateOfDriverLicense().getMonthValue();
			Integer dayOfDateOfDriverLicense = person.getDateOfDriverLicense().getDayOfMonth();
			Date dateOfDriverLicense = new Date(yearOfDateOfDriverLicense - 1900, monthOfDateOfDriverLicense - 1, dayOfDateOfDriverLicense);
			preStatement.setDate(5, dateOfDriverLicense);
			preStatement.executeUpdate();
		}catch (Exception e) {
            System.out.println("This is something you have not add in postgresql library to classpath!");
            e.printStackTrace();
        
        }finally{
            try {
            	connection.close();
            } catch (SQLException e) {
            }
        }
	}
}

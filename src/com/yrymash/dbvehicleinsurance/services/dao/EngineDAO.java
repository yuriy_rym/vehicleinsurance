package com.yrymash.dbvehicleinsurance.services.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.yrymash.dbvehicleinsurance.brands.Brand;
import com.yrymash.dbvehicleinsurance.engines.Engine;

public class EngineDAO extends BaseDAO{
	
	Connection connection = null;

	public EngineDAO(){
	}
	
	public List<Engine> getEnginesFromDB(){
		
		Engine engine = null;
		List<Engine> engines = new ArrayList<Engine>();
			
		try{
			connection = getConnectionToDB();
            Statement statement = connection.createStatement();
            ResultSet resultEngine = statement.executeQuery("SELECT * FROM \"Engine\" order by id_engine asc");
            while(resultEngine.next()){
            int idEngine = resultEngine.getInt("id_engine");
   	 		String brandEngine = resultEngine.getString("brand_engine");
   	 		String fueltypEngine = resultEngine.getString("fueltyp_engine");
   	 		int extentEngine = resultEngine.getInt("extent_engine");
   	 		int powerEngine = resultEngine.getInt("power_engine");

   	 	engine = new Engine(
   	 				brandEngine,
   	 				fueltypEngine,
   	 				idEngine,
   	 				extentEngine,
   	 				powerEngine);
   	 	engines.add(engine);
				}
            
			}catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
	        return engines;
		}
	
		public Engine getEngineById(Integer id){
		 
		 Engine currentEngine = null;
		 
		 try{
		 connection = getConnectionToDB();
		 PreparedStatement preStatement = connection.prepareStatement("SELECT * FROM \"Engine\" where id_engine = ?"); 
		 preStatement.setInt(1, id);
		 ResultSet resultEngine = preStatement.executeQuery(); 
		 while(resultEngine.next()){
			 	int idEngine = resultEngine.getInt("id_engine");
	   	 		String brandEngine = resultEngine.getString("brand_engine");
	   	 		String fueltypEngine = resultEngine.getString("fueltyp_engine");
	   	 		int extentEngine = resultEngine.getInt("extent_engine");
	   	 		int powerEngine = resultEngine.getInt("power_engine");
	   	 		currentEngine = new Engine(
	   	 			brandEngine,
   	 				fueltypEngine,
   	 				idEngine,
   	 				extentEngine,
   	 				powerEngine);	
		 		}
		 }catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
		 return currentEngine;
	}
	
	public void insertEngineIntoDB(Engine engine){
		
		try{
			connection = getConnectionToDB();
			PreparedStatement preStatement = connection.prepareStatement("INSERT INTO \"Engine\""
                + "(ID_ENGINE, BRAND_ENGINE, FUELTYP_ENGINE, EXTENT_ENGINE, POWER_ENGINE)" + "VALUES"
                + "(?, ?, ?, ?, ?)");
			preStatement.setInt(1, engine.getId());
			preStatement.setString(2, engine.getBrand());
			preStatement.setString(3, engine.getFuelType());
			preStatement.setInt(4, engine.getExtent());
			preStatement.setInt(5, engine.getPower());
			preStatement.executeUpdate();
		}catch (Exception e) {
            System.out.println("This is something you have not add in postgresql library to classpath!");
            e.printStackTrace();
        
        }finally{
            try {
            	connection.close();
            } catch (SQLException e) {
            }
        }
	}
}

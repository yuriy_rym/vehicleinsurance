package com.yrymash.dbvehicleinsurance.services.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.yrymash.dbvehicleinsurance.brands.Brand;

public class BrandDAO extends BaseDAO{

	Connection connection = null;
	
	public BrandDAO(){
	}
	
	public List<Brand> getBrandsFromDB(){
		
		Brand brand = null;
		List<Brand> brands = new ArrayList<Brand>();
		
		try{
			connection = getConnectionToDB();
            Statement statement = connection.createStatement();
            ResultSet resultBrand = statement.executeQuery("SELECT * FROM \"Brand\" order by id_brand asc");
            while(resultBrand.next()){
            	int idBrand = resultBrand.getInt("id_brand");
   	 			String nameBrand = resultBrand.getString("name_brand");
   	 			String homeCountryBrand = resultBrand.getString("homecountry_brand");
   	 			brand = new Brand(
   	 				nameBrand,
   	 				homeCountryBrand,
   	 				idBrand);
   	 			brands.add(brand);
			}
            
			}catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
	        return brands;
		}
            
	 public Brand getBrandById(Integer id){
		 
		 Brand currentBrand = null;
		 
		 try{
			 connection = getConnectionToDB();
			 PreparedStatement preStatement = connection.prepareStatement("SELECT * FROM \"Brand\" where id_brand = ?"); 
			 preStatement.setInt(1, id);
			 ResultSet resultBrand = preStatement.executeQuery(); 
			 while(resultBrand.next()){
				 int idBrand = resultBrand.getInt("id_brand");
	   	 		 String nameBrand = resultBrand.getString("name_brand");
	   	 		 String homeCountryBrand = resultBrand.getString("homecountry_brand");
	   	 		 currentBrand = new Brand(
	   	 				nameBrand,
	   	 				homeCountryBrand,
	   	 				idBrand); 
		 		 }
		 }catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
		 return currentBrand;
	}
	 
	public void insertBrandIntoDB(Brand brand){
		
		try{
			connection = getConnectionToDB();
			PreparedStatement statement = connection.prepareStatement("INSERT INTO \"Brand\""
                + "(ID_BRAND, NAME_BRAND, HOMECOUNTRY_BRAND)" + "VALUES"
                + "(?, ?, ?)");
			statement.setInt(1, brand.getId());
			statement.setString(2, brand.getName());
			statement.setString(3, brand.getHomeCountry());
			statement.executeUpdate();
		}catch (Exception e) {
            System.out.println("This is something you have not add in postgresql library to classpath!");
            e.printStackTrace();
        
        }finally{
            try {
            	connection.close();
            } catch (SQLException e) {
            }
        }
	}
}

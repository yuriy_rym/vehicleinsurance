package com.yrymash.dbvehicleinsurance.services.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.yrymash.dbvehicleinsurance.brands.Brand;
import com.yrymash.dbvehicleinsurance.placesofregistration.City;
import com.yrymash.dbvehicleinsurance.placesofregistration.Locality;
import com.yrymash.dbvehicleinsurance.placesofregistration.Town;
import com.yrymash.dbvehicleinsurance.placesofregistration.Village;

public class PlaceOfRegistrationDAO extends BaseDAO{
	
	Connection connection = null;

	public PlaceOfRegistrationDAO(){
	}
	
	public List<Locality> getPlacesOfRegistrationFromDB(){
		
		Locality placeOfRegistration = null;
		List<Locality> placesOfRegistrations = new ArrayList<Locality>();
				
		try{
			connection = getConnectionToDB();
            Statement statement = connection.createStatement();
            ResultSet resultPlaceOfRegistration = statement.executeQuery("SELECT * FROM \"Place_of_registration\" order by id_place_of_registration asc");
            while(resultPlaceOfRegistration.next()){
            	int idPlaceOfRegistration = resultPlaceOfRegistration.getInt("id_place_of_registration");
   	 			String namePlaceOfRegistration = resultPlaceOfRegistration.getString("name_place_of_registration");
   	 			int populationPlaceOfRegistration = resultPlaceOfRegistration.getInt("population_place_of_registration");
   	 			String typePlaceOfRegistration = resultPlaceOfRegistration.getString("type_place_of_registration");
   	 		

   	 			if(typePlaceOfRegistration.trim().equals("city")){
   	 				placeOfRegistration = new City(
   	 						namePlaceOfRegistration,
   	 						populationPlaceOfRegistration,
   	 						idPlaceOfRegistration,
   	 						typePlaceOfRegistration
   	 						);
				} else if(typePlaceOfRegistration.trim().equals("town")){
							placeOfRegistration = new Town(	
							namePlaceOfRegistration,
							populationPlaceOfRegistration,
							idPlaceOfRegistration,
							typePlaceOfRegistration
							);	
				}else if(typePlaceOfRegistration.trim().equals("village")){
							placeOfRegistration = new Village(
							namePlaceOfRegistration,
							populationPlaceOfRegistration,
							idPlaceOfRegistration,
							typePlaceOfRegistration
							);
				}
   	 	 		placesOfRegistrations.add(placeOfRegistration);
            }
			}catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
	        return placesOfRegistrations;
		}
	
	public Locality getPlaceOfRegistrationById(Integer id){
		 
		 Locality currentPlaceOfRegistration = null;
		 
		 try{
			 connection = getConnectionToDB();
			 PreparedStatement preStatement = connection.prepareStatement("SELECT * FROM \"Place_of_registration\" where id_place_of_registration = ?"); 
			 preStatement.setInt(1, id);
			 ResultSet resultPlaceOfRegistration = preStatement.executeQuery(); 
			 while(resultPlaceOfRegistration.next()){
			 	int idPlaceOfRegistration = resultPlaceOfRegistration.getInt("id_place_of_registration");
	   	 		String namePlaceOfRegistration = resultPlaceOfRegistration.getString("name_place_of_registration");
	   	 		int populationPlaceOfRegistration = resultPlaceOfRegistration.getInt("population_place_of_registration");
	   	 		String typePlaceOfRegistration = resultPlaceOfRegistration.getString("type_place_of_registration");
	   	 		if(typePlaceOfRegistration.trim().equals("city")){
	   	 			currentPlaceOfRegistration = new City(
	   	 			namePlaceOfRegistration,
	   	 			populationPlaceOfRegistration,
	   	 			idPlaceOfRegistration,
	   	 			typePlaceOfRegistration
	   	 			);
					} else if(typePlaceOfRegistration.trim().equals("town")){
					currentPlaceOfRegistration = new Town(	
					namePlaceOfRegistration,
		   	 		populationPlaceOfRegistration,
		   	 		idPlaceOfRegistration,
		   	 		typePlaceOfRegistration
		   	 		);	
					}else if(typePlaceOfRegistration.equals("village")){
					currentPlaceOfRegistration = new Village(
					namePlaceOfRegistration,
		   	 		populationPlaceOfRegistration,
		   	 		idPlaceOfRegistration,
		   	 		typePlaceOfRegistration
		   	 		);
					}
		 }
		 }catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
		 return currentPlaceOfRegistration;
	}
	
	public void insertPlaceOfRegistrationIntoDB(Locality locality){
		
		try{
			connection = getConnectionToDB();
			PreparedStatement preStatement = connection.prepareStatement("INSERT INTO \"Place_of_registration\""
                + "(ID_PLACE_OF_REGISTRATION, NAME_PLACE_OF_REGISTRATION, POPULATION_PLACE_OF_REGISTRATION, TYPE_PLACE_OF_REGISTRATION)" + "VALUES"
                + "(?, ?, ?, ?)");
			preStatement.setInt(1, locality.getId());
			preStatement.setString(2, locality.getName());
			preStatement.setInt(3, locality.getPopulation());
			preStatement.setString(4, locality.getTypeOfLocality());
			preStatement.executeUpdate();
		}catch (Exception e) {
            System.out.println("This is something you have not add in postgresql library to classpath!");
            e.printStackTrace();
        
        }finally{
            try {
            	connection.close();
            } catch (SQLException e) {
            }
        }
	}
}

package com.yrymash.dbvehicleinsurance.services.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class BaseDAO {

	public BaseDAO(){
		
	}
	
	public Connection getConnectionToDB(){
		Connection conn = null;
        try {
        	 Class.forName("org.postgresql.Driver").newInstance();
        
            conn = DriverManager.getConnection(
                     "jdbc:postgresql://localhost:5432/VehicleInsurance", "postgres",
                     "admin");
                    
        } catch (Exception e) {
            System.out.println("This is something you have not add in postgresql library to classpath!");
            e.printStackTrace();
        }
        return conn;
	}
}

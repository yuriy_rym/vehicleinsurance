package com.yrymash.dbvehicleinsurance.services.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.yrymash.dbvehicleinsurance.brands.Brand;
import com.yrymash.dbvehicleinsurance.drivers.Person;
import com.yrymash.dbvehicleinsurance.engines.Engine;
import com.yrymash.dbvehicleinsurance.placesofregistration.Locality;
import com.yrymash.dbvehicleinsurance.vehicles.Buss;
import com.yrymash.dbvehicleinsurance.vehicles.Car;
import com.yrymash.dbvehicleinsurance.vehicles.Motorcycle;
import com.yrymash.dbvehicleinsurance.vehicles.Truck;
import com.yrymash.dbvehicleinsurance.vehicles.Vehicle;

public class VehicleDAO extends BaseDAO{
	
	Connection connection = null;

	public VehicleDAO(){
	}
	
	public List<Vehicle> getVehiclesFromDB(){
		
		Vehicle vehicle = null;
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
				
		try{
			connection = getConnectionToDB();
            Statement statement = connection.createStatement();
            ResultSet resultVehicle = statement.executeQuery("SELECT * FROM \"Vehicle\" order by id_vehicle asc");
            while(resultVehicle.next()){
            	int idVehicle = resultVehicle.getInt("id_vehicle");
            	String typeVehicle = resultVehicle.getString("type_vehicle");
            	int idBrand = resultVehicle.getInt("id_brand");
            	int yearProduction = resultVehicle.getInt("year_production");
            	int idEngine = resultVehicle.getInt("id_engine");
            	int capacity = resultVehicle.getInt("capacity");
            	int passenger = resultVehicle.getInt("passenger");
            	int idOwner = resultVehicle.getInt("id_owner");
            	int idPlaceOfRegistration = resultVehicle.getInt("id_place_of_registration");
            
            	BrandDAO brandDAO = new BrandDAO();
            	EngineDAO engineDAO = new EngineDAO();
            	PlaceOfRegistrationDAO placeOfRegistrationDAO= new PlaceOfRegistrationDAO();
            	DriverDAO driverDAO = new DriverDAO();
            
            	Brand brandVehicle = brandDAO.getBrandById(idBrand);
            	Engine engineVehicle = engineDAO.getEngineById(idEngine);
            	Person ownerVehicle = driverDAO.getOwnerById(idOwner);
            	Locality placeOfRegistrationVehicle = placeOfRegistrationDAO.getPlaceOfRegistrationById(idPlaceOfRegistration);
            
            	if(typeVehicle.trim().equals("buss")){
            		vehicle = new Buss(
            				brandVehicle,
            				yearProduction,
            				engineVehicle,
            				capacity,
            				passenger,
            				ownerVehicle,
            				placeOfRegistrationVehicle,
            				idVehicle,
            				typeVehicle
            				);
            	}else if(typeVehicle.trim().equals("car")){
					vehicle = new Car(
							brandVehicle,
							yearProduction,
							engineVehicle,
							capacity,
							passenger,
							ownerVehicle,
							placeOfRegistrationVehicle,
							idVehicle,
							typeVehicle
							);	
				}else if(typeVehicle.trim().equals("motorcycle")){
					vehicle = new Motorcycle(
							brandVehicle,
							yearProduction,
							engineVehicle,
							capacity,
							passenger,
							ownerVehicle,
							placeOfRegistrationVehicle,
							idVehicle,
							typeVehicle
							);
				}else if(typeVehicle.trim().equals("truck")){
					vehicle = new Truck(
							brandVehicle,
							yearProduction,
							engineVehicle,
							capacity,
							passenger,
							ownerVehicle,
							placeOfRegistrationVehicle,
							idVehicle,
							typeVehicle
							);
					}
   	 			vehicles.add(vehicle);
            	}
			}catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
	        return vehicles;
		}
	
 public Vehicle getVehicleById(Integer id){
		 
		 Vehicle currentVehicle = null;
		 
		 try{
			 connection = getConnectionToDB();
			 PreparedStatement preStatement = connection.prepareStatement("SELECT * FROM \"Vehicle\" where id_vehicle = ?"); 
			 preStatement.setInt(1, id);
			 ResultSet resultVehicle = preStatement.executeQuery(); 
			 while(resultVehicle.next()){
				 int idVehicle = resultVehicle.getInt("id_vehicle");
				 String typeVehicle = resultVehicle.getString("type_vehicle");
				 int idBrand = resultVehicle.getInt("id_brand");
				 int yearProduction = resultVehicle.getInt("year_production");
				 int idEngine = resultVehicle.getInt("id_engine");
				 int capacity = resultVehicle.getInt("capacity");
				 int passenger = resultVehicle.getInt("passenger");
				 int idOwner = resultVehicle.getInt("id_owner");
				 int idPlaceOfRegistration = resultVehicle.getInt("id_place_of_registration");
	            
				 BrandDAO brandDAO = new BrandDAO();
				 EngineDAO engineDAO = new EngineDAO();
				 PlaceOfRegistrationDAO placeOfRegistrationDAO= new PlaceOfRegistrationDAO();
				 DriverDAO driverDAO = new DriverDAO();
	           
				 Brand brandVehicle = brandDAO.getBrandById(idBrand);
				 Engine engineVehicle = engineDAO.getEngineById(idEngine);
				 Person ownerVehicle = driverDAO.getOwnerById(idOwner);
				 Locality placeOfRegistrationVehicle = placeOfRegistrationDAO.getPlaceOfRegistrationById(idPlaceOfRegistration);
	            
	   	 	 	if(typeVehicle.trim().equals("buss")){
	   	 		 	currentVehicle = new Vehicle(
	   	 			brandVehicle,
	   	 			yearProduction,
	   	 			engineVehicle,
	   	 			capacity,
	   	 			passenger,
	   	 			ownerVehicle,
	   	 			placeOfRegistrationVehicle,
	   	 			idVehicle,
	   	 			typeVehicle
	   	 			);
					} else if(typeVehicle.trim().equals("car")){
						currentVehicle = new Car(
						brandVehicle,
		   	 			yearProduction,
		   	 			engineVehicle,
		   	 			capacity,
		   	 			passenger,
		   	 			ownerVehicle,
		   	 			placeOfRegistrationVehicle,
		   	 			idVehicle,
		   	 			typeVehicle
		   	 		);	
					}else if(typeVehicle.trim().equals("motorcycle")){
						currentVehicle = new Motorcycle(
						brandVehicle,
		   	 			yearProduction,
		   	 			engineVehicle,
		   	 			capacity,
		   	 			passenger,
		   	 			ownerVehicle,
		   	 			placeOfRegistrationVehicle,
		   	 			idVehicle,
		   	 			typeVehicle
		   	 		);
					}else if(typeVehicle.trim().equals("truck")){
						currentVehicle = new Truck(
						brandVehicle,
		   	 			yearProduction,
		   	 			engineVehicle,
		   	 			capacity,
		   	 			passenger,
		   	 			ownerVehicle,
		   	 			placeOfRegistrationVehicle,
		   	 			idVehicle,
		   	 			typeVehicle
					);
					}
	            }
		 		
		 }catch (Exception e) {
	            System.out.println("This is something you have not add in postgresql library to classpath!");
	            e.printStackTrace();
	        
	        }finally{
	            try {
	            	connection.close();
	            } catch (SQLException e) {
	            }
	        }
		 return currentVehicle;
	}
 
 	public void insertVehicleIntoDB(int idVehicle, String typeVehicle, int idBrand, int yearProduction, int idEngine, int capacity, int passenger, int idOwner, int idPlaceOfRegistration ){
		
		try{
			connection = getConnectionToDB();
			Statement statement = connection.createStatement();
			String insertVehicle = "INSERT INTO \"Vehicle\""
                + "(ID_VEHICLE, TYPE_VEHICLE, ID_BRAND, YEAR_PRODUCTION, ID_ENGINE, CAPACITY, PASSENGER, ID_OWNER, ID_PLACE_OF_REGISTRATION)" + "VALUES"
                + "(idVehicle, typeVehicle, idBrand, yearProduction, idEngine, capacity, passenger, idOwner, idPlaceOfRegistration)";
			statement.executeUpdate(insertVehicle);
		}catch (Exception e) {
         System.out.println("This is something you have not add in postgresql library to classpath!");
         e.printStackTrace();
     
		}finally{
		try {
         	connection.close();
         } catch (SQLException e) {
         	}
		}
	}
}

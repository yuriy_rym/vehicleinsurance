package com.yrymash.dbvehicleinsurance.services.vehicleinsurance;

import com.yrymash.dbvehicleinsurance.vehicles.Vehicle;



public class BussInsurance extends VehicleInsurance{

	public BussInsurance(Vehicle buss){
		super(buss);
	}
	
	@Override
	public Double getInsuranceCoefficient(){
		Double insuranceBuss = null;
		if(vehicle.getYearProduction()  < 2000 && vehicle.getEngine().getExtent() > 4100 && vehicle.getCapacity() > 900 && vehicle.getPassenger() > 21){
			insuranceBuss = 2.7;
		} else {
			insuranceBuss = 1.9;
		}
		return insuranceBuss * calculateDriverExperienceCoefficient() * calculatePlaceOfRegistrationCoefficient() * calculateBrandCoefficient();
	}
}

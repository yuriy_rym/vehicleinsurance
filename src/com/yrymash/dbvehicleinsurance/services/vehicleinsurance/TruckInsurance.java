package com.yrymash.dbvehicleinsurance.services.vehicleinsurance;

import com.yrymash.dbvehicleinsurance.vehicles.Vehicle;

public class TruckInsurance extends VehicleInsurance {

	public TruckInsurance(Vehicle truck){
		super(truck);
	}
	
	public Double getInsuranceCoefficient(){
		Double insuranceTruck = null;
		if(vehicle.getYearProduction() < 2000 && vehicle.getEngine().getExtent() > 5500 && vehicle.getCapacity() > 5000 && vehicle.getPassenger() > 2){
			insuranceTruck = 3.9;
		} else {
			insuranceTruck = 2.8;
		}
		return insuranceTruck * calculateDriverExperienceCoefficient() * calculatePlaceOfRegistrationCoefficient() * calculateBrandCoefficient();
	}
}

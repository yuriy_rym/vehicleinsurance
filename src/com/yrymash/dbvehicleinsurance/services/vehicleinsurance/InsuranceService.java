package com.yrymash.dbvehicleinsurance.services.vehicleinsurance;

import java.util.Iterator;
import java.util.List;

import com.yrymash.dbvehicleinsurance.vehicles.Buss;
import com.yrymash.dbvehicleinsurance.vehicles.Car;
import com.yrymash.dbvehicleinsurance.vehicles.Motorcycle;
import com.yrymash.dbvehicleinsurance.vehicles.Truck;
import com.yrymash.dbvehicleinsurance.vehicles.Vehicle;


public class InsuranceService {

private List<Vehicle> vehicles;
	
	public InsuranceService(List<Vehicle> machines){
		this.vehicles = machines;
	}
		
	public Double getInsuranceCofficient(){
		Double insuranceVehicle = 0.0;
		Iterator<Vehicle> iter = vehicles.iterator();
		while(iter.hasNext()){
			Vehicle currentVehicle = iter.next();
			if(currentVehicle instanceof Car){
				CarInsurance insuranceCar = new CarInsurance(currentVehicle);
				insuranceVehicle += insuranceCar.getInsuranceCoefficient();
			} else if(currentVehicle instanceof Truck){
				TruckInsurance insuranceTruck = new TruckInsurance(currentVehicle);
				insuranceVehicle += insuranceTruck.getInsuranceCoefficient();
			} else if(currentVehicle instanceof Buss){
				BussInsurance insuranceBuss = new BussInsurance(currentVehicle);
				insuranceVehicle +=insuranceBuss.getInsuranceCoefficient();
			} else if(currentVehicle instanceof Motorcycle){
				MotorcycleInsurance insuranceMotorcycle = new MotorcycleInsurance(currentVehicle);
				insuranceVehicle +=insuranceMotorcycle.getInsuranceCoefficient();
			}	
				
		}
		return insuranceVehicle;
	}
}

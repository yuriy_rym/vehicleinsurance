package com.yrymash.dbvehicleinsurance.services.vehicleinsurance;

import java.time.LocalDate;

import com.yrymash.dbvehicleinsurance.vehicles.Vehicle;



public class VehicleInsurance {

protected Vehicle vehicle;
	
	public VehicleInsurance(Vehicle vehicle){
		this.vehicle = vehicle;
	}
	
	public Integer calculateDriverExperience(){
		LocalDate dateOtGetDriverLicense = vehicle.getOwner().getDateOfDriverLicense();
		LocalDate today = LocalDate.now();
		Integer driverExperience = today.getYear() - dateOtGetDriverLicense.getYear();
		return driverExperience;
	}
	
	public Double calculateDriverExperienceCoefficient(){
		Integer driverExperience = calculateDriverExperience();
		Double driverExperienceCoefficient;
		if(driverExperience < 5){
			driverExperienceCoefficient = 1.5;
		}else{
			driverExperienceCoefficient = 0.8;
		}
		return driverExperienceCoefficient;
	}
	
	public Double calculatePlaceOfRegistrationCoefficient(){
		Integer populationOfPlaceOfregistration = vehicle.getPlaceOfRegistration().getPopulation();
		Double placeOfRegistrationCoefficient;
		if(populationOfPlaceOfregistration < 5000){
			placeOfRegistrationCoefficient = 1.2;		
		}else if(populationOfPlaceOfregistration < 10000){
			placeOfRegistrationCoefficient = 1.6;	
		}else{
			placeOfRegistrationCoefficient = 1.9;
		}
		return placeOfRegistrationCoefficient;
	}
	
	public Double calculateBrandCoefficient(){
		Double brandCoefficient;
		String nameBrandOfVehicle = vehicle.getBrand().getName();
		if(nameBrandOfVehicle.equals("Cermany")){
			brandCoefficient = 0.9;
		}else{
			brandCoefficient = 1.0;
		}
		return brandCoefficient;
	}
	
	public Double getInsuranceCoefficient(){
		return 1.0;
	}
}

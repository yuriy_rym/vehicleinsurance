package com.yrymash.dbvehicleinsurance.services.vehicleinsurance;

import com.yrymash.dbvehicleinsurance.vehicles.Vehicle;

public class CarInsurance extends VehicleInsurance {

	public CarInsurance(Vehicle car){
		super(car);
	}
	
	@Override
	public Double getInsuranceCoefficient(){
		Double insuranceCar = null;
		if(vehicle.getYearProduction()< 2000 && vehicle.getEngine().getExtent() > 2000 && vehicle.getCapacity() > 100 && vehicle.getPassenger() > 5){
			insuranceCar = 1.5;
		} else {
			insuranceCar = 0.9;
		}
		return insuranceCar * calculateDriverExperienceCoefficient() * calculatePlaceOfRegistrationCoefficient() * calculateBrandCoefficient();
	}
}

package com.yrymash.dbvehicleinsurance.services.vehicleinsurance;

import com.yrymash.dbvehicleinsurance.vehicles.Vehicle;

public class MotorcycleInsurance extends VehicleInsurance{

	public MotorcycleInsurance(Vehicle motorcycle){
		super(motorcycle);
	}

	public Double getInsuranceCoefficient(){
		Double insuranceMotorcycle = null;
		if(vehicle.getYearProduction()  < 2000 && vehicle.getEngine().getExtent() > 1200 && vehicle.getCapacity() > 50 && vehicle.getPassenger() > 1){
			insuranceMotorcycle = 1.8;
		} else {
			insuranceMotorcycle = 1.5;
		}
		return insuranceMotorcycle * calculateDriverExperienceCoefficient() * calculatePlaceOfRegistrationCoefficient() * calculateBrandCoefficient();
	}
}

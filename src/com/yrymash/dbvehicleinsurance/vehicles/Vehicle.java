package com.yrymash.dbvehicleinsurance.vehicles;

import com.yrymash.dbvehicleinsurance.brands.Brand;
import com.yrymash.dbvehicleinsurance.drivers.Person;
import com.yrymash.dbvehicleinsurance.engines.Engine;
import com.yrymash.dbvehicleinsurance.placesofregistration.Locality;

public class Vehicle {

		protected Brand brand = null;
		protected Integer yearProduction;
		protected Engine engine = null;
		protected Integer capacity;
		protected Integer passenger;
		protected Person owner = null;
		protected Locality placeOfRegistration = null;
		protected Integer id;
		protected String typeVehicle;
		
		public Vehicle(){
		}
		
		public Vehicle(Brand brand, Integer yearProduction, Engine engine,
				Integer capacity, Integer passenger, Person owner, Locality placeOfRegistration, Integer id, String typeVehicle) {
			this.brand = brand;
			this.yearProduction = yearProduction;
			this.engine = engine;
			this.capacity = capacity;
			this.passenger = passenger;
			this.owner = owner;
			this.placeOfRegistration = placeOfRegistration;
			this.id = id;
			this.typeVehicle = typeVehicle;
		}

		public String getTypeVehicle() {
			return typeVehicle;
		}

		public void setTypeVehicle(String typeVehicle) {
			this.typeVehicle = typeVehicle;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Locality getPlaceOfRegistration() {
			return placeOfRegistration;
		}

		public void setPlaceOfRegistration(Locality placeOfRegistration) {
			this.placeOfRegistration = placeOfRegistration;
		}

		public Engine getEngine() {
			return engine;
		}

		public void setEngine(Engine engine) {
			this.engine = engine;
		}

		public Brand getBrand() {
			return brand;
		}

		public void setBrand(Brand brand) {
			this.brand = brand;
		}

		public int getYearProduction() {
			return yearProduction;
		}

		public void setYearProduction(Integer yearProduction) {
			this.yearProduction = yearProduction;
		}

		public int getCapacity() {
			return capacity;
		}

		public void setCapacity(Integer capacity) {
			this.capacity = capacity;
		}

		public int getPassenger() {
			return passenger;
		}

		public void setPassenger(Integer passenger) {
			this.passenger = passenger;
		}
		
		public Person getOwner() {
			return owner;
		}

		public void setOwner(Person owner) {
			this.owner = owner;
		}
		
		public String toString(){
			return id.toString() + typeVehicle + "#" + brand.getName() + "#" + yearProduction.toString() + "#" + capacity.toString() + "#" + passenger.toString() + "#" + engine.getId() + "#" + placeOfRegistration.getId() + "#" + owner.getId() + "#";		
		}
		
		public void print(){
			System.out.println("[Vehicle]: id = " + id + ", type of vehicle = " + typeVehicle + ", brand = " + brand.toString() + ", yearProduction = " + yearProduction + ", engine = " + 
					engine.toString() + ", capacity = " + capacity + ", passenger = " + passenger + 
					", place of registration = " + placeOfRegistration.toString() + "owner = " + owner.toString() +
					".");
		}
}

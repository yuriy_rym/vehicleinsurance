package com.yrymash.dbvehicleinsurance.vehicles;

import com.yrymash.dbvehicleinsurance.brands.Brand;
import com.yrymash.dbvehicleinsurance.drivers.Person;
import com.yrymash.dbvehicleinsurance.engines.Engine;
import com.yrymash.dbvehicleinsurance.placesofregistration.Locality;



public class Truck extends Vehicle {

	public Truck(){
	}
	
	public Truck(Brand brand, Integer yearProduction, Engine engine,
			Integer capacity, Integer passenger, Person owner, Locality placeOfRegistration, Integer id, String typeVehicle) {
		super(brand, yearProduction, engine, capacity, passenger, owner, placeOfRegistration, id, typeVehicle);
	}
	
	public String toString(){
		return super.toString();
	}
}

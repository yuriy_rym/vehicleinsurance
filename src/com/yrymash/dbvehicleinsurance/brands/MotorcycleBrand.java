package com.yrymash.dbvehicleinsurance.brands;

public class MotorcycleBrand extends Brand{

	public MotorcycleBrand(){
	}

	public MotorcycleBrand(String name, String homeCountry, Integer id) {
		super(name, homeCountry, id);
	}
	
}

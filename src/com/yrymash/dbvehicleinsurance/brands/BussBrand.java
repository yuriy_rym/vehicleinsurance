package com.yrymash.dbvehicleinsurance.brands;


public class BussBrand extends Brand{

	public BussBrand(){
	}

	public BussBrand(String name, String homeCountry, Integer id) {
		super(name, homeCountry, id);
	}
	
}

package com.yrymash.dbvehicleinsurance.brands;

public class Brand {

	protected String name = null;
	protected String homeCountry = null;
	protected Integer id;
	
	public Brand(){
	}
	
	public Brand(String name, String homeCountry, Integer id){
		this.name = name;
		this.homeCountry = homeCountry;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHomeCountry() {
		return homeCountry;
	}

	public void setHomeCountry(String homeCountry) {
		this.homeCountry = homeCountry;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String toString(){
		return id.toString() + "#" + name + "#" + homeCountry + "#";
	}
	
	public static Brand fromString(String str){
		String[] parametresOfBrand = str.split("#");
		Integer idBrand = Integer.parseInt(parametresOfBrand[0]);
		return new Brand(
				parametresOfBrand[1], //name
				parametresOfBrand[2], //home country
				idBrand
						);
	}
	
	public void print(){
		System.out.println("[Brand]: id = " + id + ", name = " + name + ", country of production = " + homeCountry);
	}
}

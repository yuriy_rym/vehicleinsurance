package com.yrymash.dbvehicleinsurance.brands;

public class CarBrand extends Brand{

	public CarBrand(){
	}

	public CarBrand(String name, String homeCountry, Integer id) {
		super(name, homeCountry, id);
	}
}

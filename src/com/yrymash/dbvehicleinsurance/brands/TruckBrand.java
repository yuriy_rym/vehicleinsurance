package com.yrymash.dbvehicleinsurance.brands;

public class TruckBrand extends Brand{

	public TruckBrand(){
	}
	
	public TruckBrand(String name, String homeCountry, Integer id){
		super(name,homeCountry, id);
	}
}

package com.yrymash.dbvehicleinsurance.engines;

public class TruckEngine extends Engine{

	public TruckEngine(){
	}
	
	public TruckEngine(String brand, String fuelType, Integer extent, Integer power, Integer id){
		super(brand, fuelType, extent, power, id);
	}
	
	@Override
	public String toString(){
		return super.toString();
	}
	
}

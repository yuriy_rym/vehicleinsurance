package com.yrymash.dbvehicleinsurance.engines;

public class Engine {

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public Integer getExtent() {
		return extent;
	}

	public void setExtent(Integer extent) {
		this.extent = extent;
	}

	public Integer getPower() {
		return power;
	}

	public void setPower(Integer power) {
		this.power = power;
	}

		protected String brand = null;
		protected String fuelType = null;
		protected Integer extent = null;
		protected Integer power = null;
		protected Integer id;
		
		
		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Engine(){
		}
		
		public Engine(String brand, String fuelType, Integer extent , Integer power, Integer id){
			this.brand = brand;
			this.fuelType = fuelType;
			this.extent = extent;
			this.power = power;
			this.id = id;
		}
		
		public String toString(){
			return id.toString() + "#" + brand + "#" + fuelType + "#" + extent.toString() + "#" + power.toString() + "#";		
		}
		
		public void print(){
			System.out.println("[Engine]: id = " + id + ", fuelType = " + fuelType + ", extent = " + extent + 
					", power = " + power + ".");
		}
}

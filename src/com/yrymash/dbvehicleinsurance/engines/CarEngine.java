package com.yrymash.dbvehicleinsurance.engines;

public class CarEngine extends Engine{

	public CarEngine(){
	}
	
	public CarEngine(String brand, String fuelType, Integer extent, Integer power, Integer id){
		super(brand, fuelType, extent, power, id);
	}
	
	@Override
	public String toString(){
		return super.toString();
	}
}

package com.yrymash.dbvehicleinsurance.engines;

public class BussEngine extends Engine{

public static final String TYPE_BUSENGINE = "BusEngine";
	
	public BussEngine(){
	}
	
	public BussEngine(String brand, String fuelType, Integer extent, Integer power, Integer id){
		super(brand, fuelType, extent, power, id);
	}
	
	@Override
	public String toString(){
		return TYPE_BUSENGINE + "#" + super.toString();
	}
}

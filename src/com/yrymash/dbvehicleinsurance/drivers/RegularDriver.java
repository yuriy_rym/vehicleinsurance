package com.yrymash.dbvehicleinsurance.drivers;

import java.time.LocalDate;

public class RegularDriver extends Person{

	public static final String TYPE_DRIVER = "Regular";

	public RegularDriver(LocalDate birthday, LocalDate dateOfDriverLicense,
			String firstName, String lastName, Integer id) {
		super(birthday, dateOfDriverLicense, firstName, lastName, id);
		
	}
	
	public String toString(){
		return TYPE_DRIVER + "#" + super.toString();
	}
}

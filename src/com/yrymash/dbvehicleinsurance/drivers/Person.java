package com.yrymash.dbvehicleinsurance.drivers;

import java.time.LocalDate;

public class Person {

		protected LocalDate birthday;
		protected LocalDate dateOfDriverLicense;
		protected String firstName;
		protected String lastName;
		protected Integer id;
		
		public Person(LocalDate birthday, LocalDate dateOfDriverLicense,
				String firstName, String lastName,Integer id) {
			this.birthday = birthday;
			this.dateOfDriverLicense = dateOfDriverLicense;
			this.firstName = firstName;
			this.lastName = lastName;
			this.id = id;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public LocalDate getDateOfDriverLicense() {
			return dateOfDriverLicense;
		}
		
		public LocalDate getBirthday() {
			return birthday;
		}
		
		public void setBirthday(LocalDate birthday) {
			this.birthday = birthday;
		}

		public void setDateOfDriverLicense(LocalDate dateOfDriverLicense) {
			this.dateOfDriverLicense = dateOfDriverLicense;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		
		public String toString(){
			return id.toString() + "#" + birthday.toString() + "#" + dateOfDriverLicense.toString() + 
					"#" + firstName + "#" + lastName + "#";
		}
		
		public void print(){
			System.out.println("[Owner]: id = " + id + ", firstName = " + firstName + ", lastName = " + lastName +  
					", birthday = " + birthday + ", dateOfDriverLicense = " + dateOfDriverLicense + ".");
		}
}

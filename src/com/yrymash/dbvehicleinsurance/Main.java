package com.yrymash.dbvehicleinsurance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import com.yrymash.dbvehicleinsurance.brands.Brand;
import com.yrymash.dbvehicleinsurance.drivers.*;
import com.yrymash.dbvehicleinsurance.engines.Engine;
import com.yrymash.dbvehicleinsurance.placesofregistration.Locality;
import com.yrymash.dbvehicleinsurance.services.dao.BrandDAO;
import com.yrymash.dbvehicleinsurance.services.dao.DriverDAO;
import com.yrymash.dbvehicleinsurance.services.dao.EngineDAO;
import com.yrymash.dbvehicleinsurance.services.dao.PlaceOfRegistrationDAO;
import com.yrymash.dbvehicleinsurance.services.dao.VehicleDAO;
import com.yrymash.dbvehicleinsurance.services.vehicleinsurance.InsuranceService;
import com.yrymash.dbvehicleinsurance.vehicles.Vehicle;


public class Main {

	public static void main(String[] args){
		
		
            
		BrandDAO brandDAO = new BrandDAO();
		List<Brand> brands = brandDAO.getBrandsFromDB();
		Iterator<Brand> iterB = brands.iterator();
		while(iterB.hasNext()){
			iterB.next().print();
		}
			
		
		DriverDAO driverDAO = new DriverDAO();
		List<Person> owners = driverDAO.getDriversFromDB();
		Iterator<Person> iterP = owners.iterator();
		while(iterP.hasNext()){
			iterP.next().print();
		}
		
		EngineDAO engineDAO = new EngineDAO();
		List<Engine> engines = engineDAO.getEnginesFromDB();
		Iterator<Engine> iterE = engines.iterator();
		while(iterE.hasNext()){
			iterE.next().print();
		}
		
		PlaceOfRegistrationDAO placeOfRegistrationDAO = new PlaceOfRegistrationDAO();
		List<Locality> placesOfRegistration = placeOfRegistrationDAO.getPlacesOfRegistrationFromDB();
		Iterator<Locality> iterL = placesOfRegistration.iterator();
		while(iterL.hasNext()){
			iterL.next().print();
		}
		
		VehicleDAO vehicleDAO = new VehicleDAO();
		List<Vehicle> vehicles = vehicleDAO.getVehiclesFromDB();
		Iterator<Vehicle> iterV = vehicles.iterator();
		while(iterV.hasNext()){
			iterV.next().print();
		}
		
		InsuranceService insuranceServise = new InsuranceService(vehicles);
		System.out.println("Insurance cofficient of vehicles = " + insuranceServise.getInsuranceCofficient());
	}

}

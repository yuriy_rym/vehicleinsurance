package com.yrymash.dbvehicleinsurance.placesofregistration;

public class Locality {

	protected String name = null;
	protected Integer population = null;
	protected Integer id;
	protected String typeOfLocality;
	
	public Locality(String name, Integer population, Integer id, String typeOfLocatity) {
		this.name = name;
		this.population = population;
		this.id = id;
		this.typeOfLocality = typeOfLocality;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPopulation() {
		return population;
	}
	public void setPopulation(Integer population) {
		this.population = population;
	}
	
	public String getTypeOfLocality() {
		return typeOfLocality;
	}
	public void setTypeOfLocality(String typeOfLocality) {
		this.typeOfLocality = typeOfLocality;
	}
	
	public String toString(){
		return id.toString() + "#" + name + "#" + population.toString() + typeOfLocality +"#";		
	}
	
	public void print(){
		System.out.println("[Place of Registration]: id = " + id + ", name = " + name + ", population = " + population + ", type of place of registration = " + typeOfLocality + ".");
	}
}

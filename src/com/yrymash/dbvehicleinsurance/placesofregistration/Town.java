package com.yrymash.dbvehicleinsurance.placesofregistration;

public class Town extends Locality{

	public Town(String name, Integer population, Integer id, String typeOfLocality) {
		super(name, population, id, typeOfLocality);
	}
	
	public String toString(){
		return super.toString();
	}
}
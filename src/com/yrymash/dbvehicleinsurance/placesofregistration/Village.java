package com.yrymash.dbvehicleinsurance.placesofregistration;

public class Village extends Locality{

	public Village(String name, Integer population, Integer id, String typeOfLocality) {
		super(name, population, id, typeOfLocality);
	}
	
	public String toString(){
		return super.toString();
	}
}
